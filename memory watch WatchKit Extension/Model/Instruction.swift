//
//  File.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 23/08/21.
//

import Foundation

enum Instruction: String{
    case Blue = "Blue"
    case Red = "Red"
    case Yellow = "Yellow"
    case Green = "Green"
    case DigitalCrownUp = "Crown Up"
    case DigitalCrownDown = "Crown Down"
    
    static let AllInstruction: [Instruction] = [.Blue, .Red, .Yellow, .Green, DigitalCrownUp, .DigitalCrownDown]
    
    static func randomInstruction() -> Instruction {
        let instruction: Instruction = Instruction.AllInstruction.randomElement()!
        return instruction
    }
    
    static func randomInstructions(nInstruction: Int) -> [Instruction] {
        var instructions: [Instruction] = []
        for _ in 1...nInstruction{
            let instruction = Instruction.randomInstruction()
            instructions.append(instruction)
        }
        return instructions
    }
}
