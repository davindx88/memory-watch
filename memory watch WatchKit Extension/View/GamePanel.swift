//
//  GamePanel.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 26/08/21.
//

import SwiftUI
import WatchKit
import Combine

struct GamePanel: View {
    @Binding var time: Int
    var submitInstruction: (Instruction) -> Void
    var device: WKInterfaceDevice = WKInterfaceDevice()
    @State var scrollAmount: Float = 0
    
    @State var isBlueButtonTapped = false
    @State var isRedButtonTapped = false
    @State var isGreenButtonTapped = false
    @State var isYellowButtonTapped = false
    
    func move(instruction: Instruction){
        device.play(.click)
        submitInstruction(instruction)
    }
    
    var body: some View {
        GeometryReader{ geometry in
            ZStack{
                VStack{
                    HStack{
                        ColorView(btnColor: .blue, isBtnTapped: $isBlueButtonTapped){
                            move(instruction: .Blue)
                        }
                        ColorView(btnColor: .red, isBtnTapped: $isRedButtonTapped){
                            move(instruction: .Red)
                        }
                    }
                    HStack{
                        ColorView(btnColor: .green, isBtnTapped: $isGreenButtonTapped){
                            move(instruction: .Green)
                        }
                        ColorView(btnColor: .yellow, isBtnTapped: $isYellowButtonTapped){
                            move(instruction: .Yellow)
                        }
                    }
                }
                Text("\(time)")
                    .frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .background(Color.black)
                    .cornerRadius(20)
                    .animation(.default)
            }
            .focusable(true)
            .digitalCrownRotation($scrollAmount, from: -1.5, through: 1.5, by: 0.5, sensitivity: .low, isContinuous: false, isHapticFeedbackEnabled: true)
            .onReceive(Just(scrollAmount)){ output in
                let isCrownUp = output <= -1.5
                let isCrownDown = output >= 1.5
                if !isCrownUp && !isCrownDown { return }
                
                scrollAmount = 0
                let instruction: Instruction = isCrownUp ? .DigitalCrownUp : .DigitalCrownDown
                move(instruction: instruction)
            }
        }
    }
}

struct ColorView: View {
    var btnColor: UIColor
    @Binding var isBtnTapped: Bool
    var onTapGesture: () -> Void
    
    var btnScale: CGFloat { isBtnTapped ? 0.9 : 1.0 }
    
    var body: some View{
        Color(btnColor)
            .cornerRadius(20)
            .scaleEffect(btnScale)
            .animation(.easeIn(duration: 0.1), value: btnScale)
            .onTapGesture{
                onTapGesture()
                isBtnTapped = true
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1){
                    isBtnTapped = false
                }
            }
    }
}

struct GamePanel_Previews: PreviewProvider {
    static var previews: some View {
        GamePanel(time: .constant(1)){_ in }
    }
}
