//
//  GameOverPanel.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 26/08/21.
//

import SwiftUI

struct GameOverPanel: View {
    var body: some View {
        Image(systemName: "xmark.octagon")
            .font(.system(size: 100))
            .foregroundColor(.red)
            .animation(.default)
    }
}

struct GameOverPanel_Previews: PreviewProvider {
    static var previews: some View {
        GameOverPanel()
    }
}
