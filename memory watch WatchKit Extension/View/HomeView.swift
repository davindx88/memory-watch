//
//  homeView.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 23/08/21.
//

import SwiftUI

struct homeView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct homeView_Previews: PreviewProvider {
    static var previews: some View {
        homeView()
    }
}
