//
//  InstructionPanel.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 26/08/21.
//

import SwiftUI

struct InstructionPanel: View {
    var level: Int
    var instructions: [Instruction]
    var index: Int

    var body: some View {
        VStack{
            Text("Level \(level)")
                .font(.title2)
            if index < 0{
                Text("")
            }else if index >= instructions.count{
                Text("GO")
            }else{
                if index%2==0{
                    Text(instructions[index].rawValue)
                        .animation(.default)
                        .transition(AnyTransition.opacity.animation(.easeInOut(duration:0.5)))
                }else{
                    Text(instructions[index].rawValue)
                        .animation(.default)
                        .transition(AnyTransition.opacity.animation(.easeInOut(duration:0.5)))
                }
            }
        }
    }
}

struct InstructionPanel_Previews: PreviewProvider {
    static var previews: some View {
        InstructionPanel(level: 1, instructions: [.Blue, .Red, .DigitalCrownUp], index: -1)
        InstructionPanel(level: 2, instructions: [.Blue, .Red, .DigitalCrownUp], index: 0)
        InstructionPanel(level: 3, instructions: [.Blue, .Red, .DigitalCrownUp], index: 3)
    }
}
