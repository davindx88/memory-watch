//
//  NotificationView.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 18/08/21.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
