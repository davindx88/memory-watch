//
//  memory_watchApp.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 18/08/21.
//

import SwiftUI

@main
struct memory_watchApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
