//
//  ContentView.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 18/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
