//
//  homeView.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 23/08/21.
//

import SwiftUI
import CoreLocation

struct HomeView: View {
    var body: some View {
        VStack{
            Text("Memory Watch")
                .font(.title2)
                .multilineTextAlignment(.center)
            NavigationLink(destination: GameView()){
                Text("Play Game")
            }
            .background(Color.green)
            .cornerRadius(1000)            
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
