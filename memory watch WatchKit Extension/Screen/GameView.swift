//
//  GameView.swift
//  memory watch WatchKit Extension
//
//  Created by Davin Djayadi on 23/08/21.
//

import SwiftUI
import Combine

enum GameState {
    case GivingInstruction
    case RepeatInstruction
    case GameOver
}

struct GameView: View {
    @State var gameState: GameState = GameState.GivingInstruction {
        willSet{
            switch newValue {
            case .GivingInstruction:
                ctr = -1
            case .RepeatInstruction:
                time = 10
                answerIdx = 0
            default:
                break
            }
        }
        didSet{
            switch(gameState){
            case .GivingInstruction:
                isTimerConnect = true
            case .RepeatInstruction:
                isTimerConnect = true
            case .GameOver:
                isTimerConnect = false
                DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)){
                    device.play(.failure)
                }
            }
        }
    }
    @State var level: Int = 0
    @State var instructions: [Instruction] = []
    @State var ctr: Int = 0
    
    @State var answerIdx: Int = 0
    @State var time: Int = 0
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    @State var isTimerConnect = false
    
    let device: WKInterfaceDevice = WKInterfaceDevice()
    
    var body: some View {
        ZStack{
            if gameState == .GivingInstruction{
                InstructionPanel(level: level, instructions: instructions, index: ctr)
            }else if gameState == .RepeatInstruction{
                GamePanel(time: $time, submitInstruction: self.submitInstruction)
            }else if gameState == .GameOver{
                GameOverPanel()
            }else{
                Text("Hmm...")
            }
        }
        .onReceive(timer, perform: onTimerTick)
        .onAppear(perform: onLoad)
    }
    
    func onLoad(){
        generateInstructions()
    }
    
    func onTimerTick(input: Publishers.Autoconnect<Timer.TimerPublisher>.Output){
        if !isTimerConnect { return }
        
        if gameState == .GivingInstruction {
            if ctr < instructions.count {
                device.play(.click)
                withAnimation{
                    ctr += 1
                }
            }else{ // Pindah State
                gameState = .RepeatInstruction
            }
        }else if gameState == .RepeatInstruction {
            time -= 1
            if time < 0 {
                gameState = .GameOver
            }
        }
    }
    
    func generateInstructions(){
        if level <= 0 {
            let newInstructions: [Instruction] = Instruction.randomInstructions(nInstruction: 3)
            instructions += newInstructions
        }else{
            let newInstruction: Instruction = Instruction.randomInstruction()
            instructions.append(newInstruction)
        }
        level += 1
        gameState = .GivingInstruction
    }
    
    func submitInstruction(instruction: Instruction){
        let isCorrect = instructions[answerIdx] == instruction
        if !isCorrect {
            gameState = .GameOver
            return
        }
        answerIdx += 1
        if answerIdx == instructions.count{
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)){
                device.play(.success)
                generateInstructions()
            }
        }
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView()
    }
}
