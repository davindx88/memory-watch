# Memory Watch
![](./src-readme/memory-watch-2.png)  

A native WatchOS Game App that helps **people in their spare time** to improve their **brain function**, **visual recognition**, and **short-term memory** by providing **memoization game** that memorize set of instruction with **various interaction**. Inspired by [simon](https://en.wikipedia.org/wiki/Simon_(game)) game.

## How to Run
1. clone / download this project
2. open this project in [xcode](https://developer.apple.com/xcode/)
3. build and run this project